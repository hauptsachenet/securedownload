<?php

namespace Hn\Securedownload\Controller;


use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Extbase\Domain\Model\File;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class DownloadController extends ActionController
{
    public function downloadAction(File $file)
    {
        $resource = $file->getOriginalResource();
        $feGroupIds = GeneralUtility::trimExplode(',', (string)$resource->getProperty('fe_group'));

        if (empty($feGroupIds)) {
            $this->sendFile($resource);
            exit;
        }

        if (!$GLOBALS['TSFE']->fe_user->user) {
            HttpUtility::setResponseCode(HttpUtility::HTTP_STATUS_401);
            exit;
        }

        if (!array_intersect($feGroupIds, $GLOBALS['TSFE']->fe_user->groupData['uid'])) {
            HttpUtility::setResponseCode(HttpUtility::HTTP_STATUS_401);
            exit;
        }

        $this->sendFile($resource);
        exit;
    }

    protected function sendFile(\TYPO3\CMS\Core\Resource\File $resource)
    {
        $response = new BinaryFileResponse($resource->getForLocalProcessing(false));
        $response->prepare(Request::createFromGlobals());
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $resource->getName());
        $response->send();
    }
}