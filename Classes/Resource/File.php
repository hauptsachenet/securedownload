<?php

namespace Hn\Securedownload\Resource;

use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class File extends \TYPO3\CMS\Core\Resource\File
{
    /**
     * Returns a publicly accessible URL for this file
     * When file is marked as missing or deleted no url is returned
     *
     * WARNING: Access to the file may be restricted by further means, e.g. some
     * web-based authentication. You have to take care of this yourself.
     *
     * @param bool $relativeToCurrentScript Determines whether the URL returned should be relative to the current script, in case it is relative at all (only for the LocalDriver)
     *
     * @return string
     */
    public function getPublicUrl($relativeToCurrentScript = false, $forceSecureDownload = false)
    {
        if (!$forceSecureDownload && !$this->getProperty('fe_group')) {
            return parent::getPublicUrl($relativeToCurrentScript);
        }

        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(ObjectManager::class);
        /** @var UriBuilder $uriBuilder */
        $uriBuilder = $objectManager->get(UriBuilder::class);
        $uriBuilder->initializeObject();

        $uri = $uriBuilder
            ->setArguments([
                'type' => '1493032228',
                'tx_securedownload_download' => [
                    'file' => $this->getUid()
                ]
            ])
            ->build()
        ;

        return $uri;
    }

    public function getLoginRequiered()
    {
        return !$this->getProperty('fe_group');
    }
}
