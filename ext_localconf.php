<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}


$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Core\Resource\File::class] = array(
    'className' => \Hn\Securedownload\Resource\File::class
);


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Hn.Securedownload',
    'Download',
    ['Download' => 'download'],
    ['Download' => 'download']
);