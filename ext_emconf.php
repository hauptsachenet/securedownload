<?php

$EM_CONF[$_EXTKEY] = array(
	'title' => 'securedownload',
	'description' => 'Protect downloads based on fe_groups.',
	'category' => '',
	'author' => '',
	'author_email' => '',
	'author_company' => '',
	'shy' => '',
	'priority' => '',
	'module' => '',
	'state' => 'beta',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'version' => '',
	'constraints' => array(
		'depends' => array(
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);